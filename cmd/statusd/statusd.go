package main

import (
	"flag"
	"log"
	"net/http"
	"strings"

	"git.autistici.org/ai/status"
)

var (
	addr        = flag.String("addr", ":1166", "address to listen on")
	publicAddr  = flag.String("public-addr", "127.0.0.1:1166", "address advertised to peers")
	peers       = flag.String("peers", "", "gossip peer addresses (comma-separated)")
	staticDir   = flag.String("static-dir", "./static", "path to static content")
	templateDir = flag.String("template-dir", "./templates", "path to html templates")
	appRoot     = flag.String("app-root", "/", "application base URL path")
)

func main() {
	flag.Parse()

	peerList := strings.Split(*peers, ",")
	db := status.NewDB(*publicAddr, peerList)
	go db.Run()

	http.Handle("/static/", http.StripPrefix("/static",
		http.FileServer(http.Dir(*staticDir))))

	log.Fatal(status.RunServer(db, *addr, *appRoot, *templateDir))
}
