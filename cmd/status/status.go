package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"os"
	"os/exec"
	"strings"

	"git.autistici.org/ai/status"
)

var (
	server = flag.String("server", "localhost:1166", "server to connect to")

	eventId = flag.String("id", "", "update the specified event")
	isNew   = flag.Bool("new", false, "create a new event")
	title   = flag.String("title", "", "title for the new event")
	doClose = flag.Bool("close", false, "close the event and mark it resolved")
	user    = flag.String("user", os.Getenv("USER"), "user to post as")
	doList  = flag.Bool("list", false, "list all open events")
)

func getMessage() string {
	// The update message can be passed as command-line arguments.
	msg := strings.Join(flag.Args(), " ")
	// Otherwise, invoke $EDITOR on a temporary file.
	if msg == "" {
		tmpf, err := ioutil.TempFile("", "update-")
		if err != nil {
			log.Fatalf("Could not create temporary file: %v", err)
		}
		tmpf.Close()
		defer os.Remove(tmpf.Name())

		editor := os.Getenv("EDITOR")
		if editor == "" {
			editor = "/usr/bin/vi"
		}
		log.Printf("running %s %s", editor, tmpf.Name())
		cmd := exec.Command(editor, tmpf.Name())
		cmd.Stdin = os.Stdin
		cmd.Stdout = os.Stdout
		cmd.Stderr = os.Stderr
		if err := cmd.Run(); err != nil {
			log.Fatalf("Update aborted: %v", err)
		}

		msgBytes, err := ioutil.ReadFile(tmpf.Name())
		if err != nil {
			log.Fatalf("Could not re-read update message: %v", err)
		}
		msg = string(msgBytes)
	}
	return msg
}

func main() {
	flag.Parse()

	// Check for conflicting options.
	if !*doList {
		if *isNew {
			if *doClose {
				log.Fatal("--new and --close can not be specified together")
			}
			if *eventId != "" {
				log.Fatal("--new and --id can not be specified together")
			}
			if *title == "" {
				log.Fatal("New issues must have a title (use the --title option)")
			}
		}
		if !*isNew && *eventId == "" {
			log.Fatal("You need to provide at least one of --new (to create a new event) or --id (to update an existing one)")
		}
	}
	if *doList && (*isNew || *eventId != "" || *doClose) {
		log.Fatal("--list can not be used with any of --new, --id or --close")
	}

	client := &http.Client{}

	if *doList {
		resp, err := client.Get(fmt.Sprintf("http://%s/api/1/get_events", *server))
		if err != nil {
			log.Fatalf("HTTP call error: %v", err)
		}
		if resp.StatusCode != 200 {
			log.Fatalf("HTTP Error: status: %s", resp.Status)
		}
		defer resp.Body.Close()

		var respData struct {
			Events []status.Event
		}
		if err := json.NewDecoder(resp.Body).Decode(&respData); err != nil {
			log.Fatalf("JSON error: %v", err)
		}

		for _, e := range respData.Events {
			if e.State != status.StateActive {
				continue
			}
			fmt.Printf("%-20d %-16s %s\n",
				e.ID,
				e.UpdatedAt.Format("2006/2/1 15:04"),
				e.Title)
		}

		return
	}

	msg := getMessage()

	var path string
	v := make(url.Values)
	v.Set("author", *user)
	v.Set("content", msg)
	if *isNew {
		v.Set("title", *title)
		path = "/api/1/admin/add"
	} else {
		v.Set("id", *eventId)
		if *doClose {
			path = "/api/1/admin/close"
		} else {
			path = "/api/1/admin/update"
		}
	}
	serverUrl := fmt.Sprintf("http://%s%s", *server, path)
	resp, err := client.Post(serverUrl, "application/x-www-form-urlencoded", strings.NewReader(v.Encode()))
	if err != nil {
		log.Fatal(err)
	}
	if resp.StatusCode != 200 {
		log.Fatalf("HTTP Error: status: %s", resp.Status)
	}
	fmt.Println("done.")
}
