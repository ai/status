package status

import (
	"encoding/json"
	"fmt"
	"html/template"
	"log"
	"net/http"
	"net/rpc"
	"path/filepath"
	"strconv"
	"strings"
)

type server struct {
	db *DB
}

func (s *server) handleAddEvent(w http.ResponseWriter, r *http.Request) {
	author := r.FormValue("author")
	content := r.FormValue("content")
	title := r.FormValue("title")
	if author == "" || content == "" || title == "" {
		log.Printf("empty params (author=%s title=%s content=%s)", author, title, content)
		http.Error(w, "Bad Request", http.StatusBadRequest)
		return
	}

	e := NewEvent(title, content, author)
	s.db.Add(e)
	fmt.Fprintf(w, "%d\n", e.ID)
}

func (s *server) handleUpdateWithState(w http.ResponseWriter, r *http.Request, state int) {
	id, err := strconv.ParseInt(r.FormValue("id"), 10, 64)
	if err != nil {
		log.Printf("bad ID '%s': %s", id, err)
		http.Error(w, "Bad Request", http.StatusBadRequest)
		return
	}
	author := r.FormValue("author")
	content := r.FormValue("content")
	if author == "" || content == "" {
		log.Printf("empty author or content")
		http.Error(w, "Bad Request", http.StatusBadRequest)
		return
	}

	if err := s.db.Update(id, content, author, state); err != nil {
		log.Printf("Update() error: %v", err)
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}
}

func (s *server) handleUpdateEvent(w http.ResponseWriter, r *http.Request) {
	s.handleUpdateWithState(w, r, StateNone)
}

func (s *server) handleCloseEvent(w http.ResponseWriter, r *http.Request) {
	s.handleUpdateWithState(w, r, StateInactive)

}

func (s *server) handleGetEvents(w http.ResponseWriter, r *http.Request) {
	out := struct {
		Events []Event
	}{s.db.AllEvents()}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(&out)
}

// RunServer starts an HTTP server exporting an API for the specified
// database. It will listen on 'addr' (which should match with the
// public address of 'db', as peers will attempt to connect to the Go
// RPC endpoint there). 'appRoot' can be used to support complex proxy
// setups by specifying the absolute URL path where the HTTP API is
// served from.
func RunServer(db *DB, addr, appRoot, templateDir string) error {
	s := &server{db}

	r := http.NewServeMux()

	adminr := http.NewServeMux()
	adminr.HandleFunc("/add", s.handleAddEvent)
	adminr.HandleFunc("/update", s.handleUpdateEvent)
	adminr.HandleFunc("/close", s.handleCloseEvent)
	r.Handle("/api/1/admin/", http.StripPrefix("/api/1/admin", adminr))

	r.HandleFunc("/api/1/get_events", s.handleGetEvents)

	appRoot = strings.TrimRight(appRoot, "/")
	indexTpl := template.Must(template.ParseFiles(filepath.Join(templateDir, "index.html")))
	r.Handle("/", http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "text/html")
		var ctx = struct {
			AppRoot string
		}{appRoot}
		indexTpl.ExecuteTemplate(w, "index.html", &ctx)
	}))

	rpc.HandleHTTP()
	http.Handle("/", r)

	return http.ListenAndServe(addr, nil)
}
