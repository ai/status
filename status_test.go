package status

import (
	"testing"

	"git.autistici.org/ale/gossip"
)

func TestEvent_Merge(t *testing.T) {
	e1 := NewEvent("outage", "test outage", "admin")
	e2 := NewEvent("outage", "test outage", "admin")
	e1.Add("update", "admin", StateInactive)

	e1.Merge(*e2)
	if len(e1.Updates) != 3 {
		t.Fatalf("Wrong number of updates (exp. 3): %+v", e1)
	}

	if e1.State != StateInactive {
		t.Fatalf("Merged event is not inactive: %+v", e1)
	}
}

func createTestDb() *DB {
	return NewDB("localhost", []string{"host1", "host2"})
}

func TestDB_Add(t *testing.T) {
	db := createTestDb()
	db.Add(NewEvent("outage", "test outage", "admin"))

	if len(db.events) != 1 {
		t.Fatalf("Event not added: %+v", db)
	}
}

func TestDB_UpdateNonExistingEventFails(t *testing.T) {
	db := createTestDb()
	err := db.Update(1, "content", "author", StateActive)
	if err == nil {
		t.Fatal("No error updating non-existing event")
	}
}

func TestDB_Update(t *testing.T) {
	db := createTestDb()
	e := NewEvent("outage", "test outage", "admin")
	db.Add(e)

	err := db.Update(e.ID, "update", "admin", StateInactive)
	if err != nil {
		t.Fatal("Update: ", err)
	}

	e2, _ := db.Get(e.ID)
	if e2.State != StateInactive {
		t.Fatalf("Update not received: %+v", db)
	}

	all := db.AllEvents()
	if len(all) != 1 {
		t.Fatalf("Bad len() of AllEvents: %v", all)
	}
	if all[0].State != StateInactive {
		t.Fatalf("AllEvents() not updated: %v", all)
	}

	updates := e2.Updates
	if updates[1].Author != "admin" || updates[1].Content != "update" {
		t.Fatalf("Update() mangled event arguments: %+v", updates)
	}
}

func TestDB_Get(t *testing.T) {
	db := createTestDb()
	e := NewEvent("outage", "test outage", "admin")
	db.Add(e)

	e2, ok := db.Get(e.ID)
	if !ok {
		t.Fatal("Event not found")
	}
	if e2.ID != e.ID {
		t.Fatalf("Returned wrong event: %+v", e2)
	}
}

func TestDB_Tick(t *testing.T) {
	db := createTestDb()
	e := NewEvent("outage1", "test outage", "admin")
	db.Add(e)

	otherdb := createTestDb()
	e2 := NewEvent("outage2", "test outage", "admin")
	otherdb.Add(e2)

	nodes := []gossip.Member{
		{
			Addr:      "host1",
			Heartbeat: 42,
			Data:      otherdb.AllEvents(),
		},
	}
	db.tick(nodes)

	if _, ok := db.Get(e2.ID); !ok {
		t.Fatal("Event outage2 not merged")
	}
	if _, ok := db.Get(e.ID); !ok {
		t.Fatal("Event outage1 disappeared")
	}

	all := db.AllEvents()
	if len(all) != 2 {
		t.Fatalf("AllEvents() failed: %v", all)
	}
}
