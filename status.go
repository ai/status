package status

import (
	"errors"
	"math/rand"
	"sort"
	"sync"
	"time"

	"git.autistici.org/ale/gossip"
)

const (
	StateNone = iota
	StateActive
	StateInactive
)

type Update struct {
	ID        int64
	Author    string
	Content   string
	State     int
	Timestamp time.Time
}

type Event struct {
	ID        int64
	Title     string
	State     int
	CreatedAt time.Time
	UpdatedAt time.Time
	ClosedAt  time.Time
	Updates   []Update
}

func newID() int64 {
	return rand.Int63()
}

// NewEvent creates a new Event with an initial status update.
func NewEvent(title, content, author string) *Event {
	e := &Event{
		ID:    newID(),
		Title: title,
	}
	e.Add(content, author, StateActive)
	return e
}

type updateList []Update

func (l updateList) Len() int      { return len(l) }
func (l updateList) Swap(i, j int) { l[i], l[j] = l[j], l[i] }
func (l updateList) Less(i, j int) bool {
	return l[i].Timestamp.Before(l[j].Timestamp)
}

func mergeUpdates(a, b []Update) []Update {
	// Deduplicate updates by ID.
	tmp := make(map[int64]Update)
	for _, updates := range [][]Update{a, b} {
		for _, u := range updates {
			tmp[u.ID] = u
		}
	}

	// Create the result list and sort it by ascending timestamp.
	var result []Update
	for _, u := range tmp {
		result = append(result, u)
	}
	sort.Sort(updateList(result))
	return result
}

// Merge the updates of 'other' into the receiver. It is assumed that
// the two Events share the same ID.
func (e *Event) Merge(other Event) {
	e.Updates = mergeUpdates(e.Updates, other.Updates)
	e.update()
}

// Update fields by applying the event updates in sequence.
func (e *Event) update() {
	e.State = StateActive
	for i, u := range e.Updates {
		// Creation time is the time of the first update.
		if i == 0 {
			e.CreatedAt = u.Timestamp
		}
		if u.State != StateNone {
			if u.State == StateInactive {
				e.ClosedAt = u.Timestamp
			}
			e.State = u.State
		}
		e.UpdatedAt = u.Timestamp
	}
}

// Add an update to this event.
func (e *Event) Add(content, author string, state int) {
	e.Updates = append(e.Updates, Update{
		ID:        newID(),
		Author:    author,
		Content:   content,
		State:     state,
		Timestamp: time.Now(),
	})
	e.update()
}

type DB struct {
	Addr  string
	Peers []string

	// Active representation of the event set. This data can't be
	// safely serialized by the gossip worker goroutine, so we use
	// a second, exportable form of it in 'exportEvents'.
	events       map[int64]*Event
	exportEvents []Event

	// Lock to protect access to 'events'.
	lock sync.Mutex
}

// NewDB creates a new database and seeds the gossip network with a
// list of initial peers. 'addr' is the public address (host:port) of
// the local node.
func NewDB(addr string, peers []string) *DB {
	return &DB{
		Addr:   addr,
		Peers:  peers,
		events: make(map[int64]*Event),
	}
}

func (db *DB) merge(otherEvents []Event) {
	for _, e := range otherEvents {
		if cur, ok := db.events[e.ID]; ok {
			cur.Merge(e)
		} else {
			db.events[e.ID] = &e
		}
	}
}

// Add a new Event to the database.
func (db *DB) Add(e *Event) {
	db.lock.Lock()
	defer db.lock.Unlock()
	db.events[e.ID] = e
	db.updateExport()
}

// Update an event.
func (db *DB) Update(id int64, content, author string, state int) error {
	db.lock.Lock()
	defer db.lock.Unlock()

	e, ok := db.events[id]
	if !ok {
		return errors.New("no such event")
	}
	e.Add(content, author, state)
	db.updateExport()
	return nil
}

// Get a specific event by ID.
func (db *DB) Get(id int64) (Event, bool) {
	db.lock.Lock()
	defer db.lock.Unlock()
	if e, ok := db.events[id]; ok {
		return *e, true
	}
	return Event{}, false
}

// AllEvents returns a copy of all the events in the database.
func (db *DB) AllEvents() []Event {
	return db.exportEvents
}

func (db *DB) updateExport() {
	events := make([]Event, 0, len(db.events))
	for _, e := range db.events {
		events = append(events, *e)
	}
	db.exportEvents = events
}

func (db *DB) tick(nodes []gossip.Member) []Event {
	db.lock.Lock()
	defer db.lock.Unlock()

	for _, m := range nodes {
		db.merge(m.Data.([]Event))
	}
	db.updateExport()
	return db.exportEvents
}

// Run the database gossip protocol, listening for updates. This
// function will not return.
func (db *DB) Run() {
	g := gossip.New(db.Addr, db.exportEvents, db.Peers)
	//g.DebugOutput = true
	go g.Run()
	for {
		<-g.C

		// Merge our events database with the ones from the
		// other nodes, and generate the goroutine-safe
		// exportable version of it.
		g.SetData(db.tick(g.GetMembers()))
	}
}
