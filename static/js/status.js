// Load the status dashboard data.


statusdash = {
    APP_ROOT: '',

    STATE_ACTIVE: 1,
    STATE_INACTIVE: 2,

    HTML_STATUS_OK: '<h1 class="global_status">Global status: <span class="label label-success">OK</span></h1>',
    HTML_STATUS_NOT_OK: '<h1 class="global_status">Global status: <span class="label label-danger">NOT OK</span></h1>'
};

fmtdate = function(t) {
    if (t) {
        return t.substr(0, 16).replace('T', ' ') + ' GMT';
    }
    return 'N/A';
};

statusdash.render_event = function(ev) {
    var tpl = $(
        '<div class="panel ' + (ev.State == statusdash.STATE_ACTIVE ? 'panel-danger' : 'panel-default') + ' event">'
            + '<div class="panel-heading event_title">' + ev.Title + '</div>'
            + '<div class="panel-body">'
            + ' <div class="row">'
            + '  <div class="col-md-6 event_created_at">Opened at ' + fmtdate(ev.CreatedAt) + '</div>'
            + '  <div class="col-md-6 event_updated_at">Last update at ' + fmtdate(ev.UpdatedAt) + '</div>'
            + ' </div>'
            + '</div>'
            + '<ul class="list-group"></ul>'
            + '</div>');
    $.each(ev.Updates, function(idx2, up) {
        var inner = $(
            '<li class="list-group-item event_update">'
                + '<div class="update_content">' + up.Content + '</div>'
                + '<div class="update_timestamp text-right">' + fmtdate(up.Timestamp) + '</div>'
                + '</li>');
        tpl.children('ul').append(inner);
    });
    return tpl;
};

statusdash.render_events = function(eventsByStatus) {
    var eventlist = $('<div class="col-md-12 events"></div>');
    var appendenvs = function(events) {
        $.each(events, function(idx, ev) {
            eventlist.append(statusdash.render_event(ev));
        });
    };
    if (eventsByStatus['open'].length > 0) {
        eventlist.append($(statusdash.HTML_STATUS_NOT_OK));
        eventlist.append($('<h2>Ongoing issues</h2>'));
        appendenvs(eventsByStatus['open']);
    } else {
        eventlist.append($(statusdash.HTML_STATUS_OK));
    }
    if (eventsByStatus['resolved'].length > 0) {
        eventlist.append($('<h2>Recently resolved issues</h2>'));
        appendenvs(eventsByStatus['resolved']);
    }
    return eventlist;
};

statusdash.split_events_by_status = function(events) {
    var out = {open: [], resolved: []};
    $.each(events, function(idx, ev) {
        if (ev.State == statusdash.STATE_ACTIVE) {
            out['open'].push(ev);
        } else {
            out['resolved'].push(ev);
        }
    });
    return out;
};

statusdash.get_events = function() {
    $.ajax({
        url: statusdash.APP_ROOT + '/api/1/get_events',
        method: 'get',
        success: function(data) {
            var events, eventsByStatus;
            events = data.Events;

            if (events) {
                // Sort events by decreasing update timestamp.
                events.sort(function(a, b) {
                    if (a.UpdatedAt > b.UpdatedAt) {
                        return -1;
                    }
                    if (b.UpdatedAt > a.UpdatedAt) {
                        return 1;
                    }
                    return 0;
                });

                eventsByStatus = statusdash.split_events_by_status(events);
            } else {
                eventsByStatus = {open:[], resolved:[]}
            }

            $('#status_row').html(statusdash.render_events(eventsByStatus));
        },
        failure: function() {
            $('#status_p').text('There has been an error retrieving the status data.');
        },
    });
};

// Trigger a reload as soon as the document is loaded.
$(function() {
    statusdash.get_events();
});

